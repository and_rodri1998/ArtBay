package com.almr.androdri98.diarie;

import com.google.firebase.auth.FirebaseUser;

/**
 * Created by androdri8 on 16/02/18.
 */

public class UserAtual {
    FirebaseUser userMail;
    String uid;

    public UserAtual() {

    }

    public UserAtual(FirebaseUser userMail, String uid) {
        this.userMail = userMail;
        this.uid = uid;
    }

    public FirebaseUser getUserMail() {
        return userMail;
    }

    public void setUserMail(FirebaseUser userMail) {
        this.userMail = userMail;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
