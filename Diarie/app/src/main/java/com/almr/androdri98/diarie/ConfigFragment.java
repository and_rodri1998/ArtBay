package com.almr.androdri98.diarie;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.firebase.auth.FirebaseAuth;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConfigFragment extends Fragment {


    public ConfigFragment() {
        // Required empty public constructor
    }

    private viewHelpers varConfig;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        Activity activity;

        if (context instanceof Activity){
            activity=(Activity) context;
            try{
                varConfig=(viewHelpers) activity;
            }
            catch (ClassCastException e){
                throw new ClassCastException(activity.toString()
                        + " deve implementar GridFeedInterface");
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_config, container, false);

        final CheckBox checkbox=(CheckBox)view.findViewById(R.id.checkConfig);
        final EditText editUser=(EditText)view.findViewById(R.id.editUser);
        editUser.setVisibility(View.INVISIBLE);
        final EditText editPass=(EditText)view.findViewById(R.id.editPass);
        editPass.setVisibility(View.INVISIBLE);
        final ImageButton btnSave=(ImageButton)view.findViewById(R.id.btn_act_conf);
        btnSave.setVisibility(View.INVISIBLE);

        Button btnLogout=(Button)view.findViewById(R.id.btnLogOut);
        btnLogout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
            FirebaseAuth.getInstance().signOut();
                varConfig.finalizarApp();
            }
        });


        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()){
                    final EditText editUser=(EditText)view.findViewById(R.id.editUser);
                    editUser.setVisibility(View.VISIBLE);
                    final EditText editPass=(EditText)view.findViewById(R.id.editPass);
                    editPass.setVisibility(View.VISIBLE);
                    final ImageButton btnSave=(ImageButton)view.findViewById(R.id.btn_act_conf);
                    btnSave.setVisibility(View.VISIBLE);
                }
                else{
                    final EditText editUser=(EditText)view.findViewById(R.id.editUser);
                    editUser.setVisibility(View.INVISIBLE);
                    final EditText editPass=(EditText)view.findViewById(R.id.editPass);
                    editPass.setVisibility(View.INVISIBLE);
                    final ImageButton btnSave=(ImageButton)view.findViewById(R.id.btn_act_conf);
                    btnSave.setVisibility(View.INVISIBLE);
                }
            }
        });

        return view;
    }

}
