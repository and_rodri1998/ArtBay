package com.almr.androdri98.diarie.modelFirebase;

/**
 * Created by androdri8 on 16/02/18.
 */

public class Posts {
    public int favoritado;
    public String imagem;
    public String nomefanart;
    public String usernamepost;
    public String idAutorPost;
    public String idPublicacao;
    public String uri;

    public Posts() {

    }

    public Posts(int favoritado, String imagem, String nomefanart, String usernamepost, String idAutorPost, String idPublicacao,String uri) {
        this.favoritado = favoritado;
        this.imagem = imagem;
        this.nomefanart = nomefanart;
        this.usernamepost = usernamepost;
        this.idAutorPost=idAutorPost;
        this.idPublicacao=idPublicacao;
        this.uri=uri;

    }

    public int getFavoritado() {
        return favoritado;
    }

    public void setFavoritado(int favoritado) {
        this.favoritado = favoritado;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public String getNomefanart() {
        return nomefanart;
    }

    public void setNomefanart(String nomefanart) {
        this.nomefanart = nomefanart;
    }

    public String getUsernamepost() {
        return usernamepost;
    }

    public void setUsernamepost(String usernamepost) {
        this.usernamepost = usernamepost;
    }

    public String getIdAutorPost() {
        return idAutorPost;
    }

    public void setIdAutorPost(String idAutorPost) {
        this.idAutorPost = idAutorPost;
    }

    public String getIdPublicacao() {
        return idPublicacao;
    }

    public void setIdPublicacao(String idPublicacao) {
        this.idPublicacao = idPublicacao;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
