package com.almr.androdri98.diarie;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.almr.androdri98.diarie.modelFirebase.Posts;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by androdri98 on 31/01/18.
 */

public class AdapterPostFeed extends RecyclerView.Adapter<AdapterPostFeed.ViewHolder>{
    private List<Posts> itemPosts;
    private Context context;

    public AdapterPostFeed(List<Posts> itemPosts, Context context) {
        this.itemPosts = itemPosts;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.post_diarie, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Posts itemPost = itemPosts.get(position);

        holder.tvUser.setText(itemPost.getUsernamepost());
        holder.imUser.setImageResource(R.drawable.perfil);
        Picasso.with(context).load(itemPost.getUri()).resize(500,500).centerCrop().into(holder.imFanart);
        holder.autorDesc.setText(itemPost.getNomefanart());

        holder.clickUser.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
//                Toast.makeText(v.getContext(),"clicou no "+itemPost.getUsernameDiarie(),Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, ViewPerfActivity.class);

//                Toast.makeText(context,"username: "+itemPost.getUsernamepost(),Toast.LENGTH_SHORT).show();

                Bundle dadosPost=new Bundle();
                dadosPost.putString("nameUser",itemPost.getUsernamepost());
                dadosPost.putString("idAutPost",itemPost.getIdAutorPost());
                intent.putExtras(dadosPost);

                context.startActivity(intent);
            }
        });

        holder.itemPostClick.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(v.getContext(), ViewPostActivity.class);
                Bundle dadosPost=new Bundle();
                dadosPost.putString("nameUser",itemPost.getUsernamepost());
                dadosPost.putString("imUser",String.valueOf(R.drawable.perfil));
                dadosPost.putString("nomeFanart",itemPost.getNomefanart());
                dadosPost.putString("uriImagem",itemPost.getUri());
                dadosPost.putInt("numFav",itemPost.getFavoritado());
                dadosPost.putString("idAutPost",itemPost.getIdAutorPost());
                dadosPost.putString("idPub",itemPost.getIdPublicacao());
                intent.putExtras(dadosPost);

                context.startActivity(intent);
//                AppCompatActivity activity =  (AppCompatActivity) v.getContext();
//                ViewPostFragment fragment = new ViewPostFragment();
//                activity.getSupportFragmentManager().beginTransaction().replace(R.id.content, fragment).addToBackStack(null).commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemPosts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView tvUser;
        ImageView imUser;
        TextView tvDesc;
        TextView autorDesc;
        ConstraintLayout clickUser;
        ConstraintLayout itemPostClick;
        ImageView imFanart;

        public ViewHolder(View itemView) {
            super(itemView);

            tvUser=(TextView)itemView.findViewById(R.id.tvUser);
            imUser=(ImageView)itemView.findViewById(R.id.imUser);
            autorDesc=(TextView)itemView.findViewById(R.id.tvNameFan);
            itemPostClick = (ConstraintLayout) itemView.findViewById(R.id.itemPost);
            clickUser =(ConstraintLayout)itemView.findViewById(R.id.headUser);
            imFanart=(ImageView)itemView.findViewById(R.id.imFanart);

        }
    }
}
