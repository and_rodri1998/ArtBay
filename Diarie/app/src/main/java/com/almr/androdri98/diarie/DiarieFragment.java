package com.almr.androdri98.diarie;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.almr.androdri98.diarie.modelFirebase.Posts;
import com.almr.androdri98.diarie.modelFirebase.Users;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class DiarieFragment extends Fragment {

    private RecyclerView recyclerView;
    private AdapterBusca adapter;
    private List<Users> listItems;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private EditText inpBusca;

    public DiarieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_diarie, container, false);

//        ImageView round = (ImageView)view.findViewById(R.id.imgPerf);
//
//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.perfil);
//        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
//        roundedBitmapDrawable.setCircular(true);
//        round.setImageDrawable(roundedBitmapDrawable);

        iniciarVars(view);
        iniciarFire();
        editEvent();

        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        listItems=new ArrayList<>();

//        for (int i=0; i<=20; i++){
//            ItemBusca listItem=new ItemBusca(
//                    "User"+(i+1),
//                    R.drawable.perfil
//            );
//
//            listItems.add(listItem);
//        }

//        mDatabase.child("users").addValueEventListener(
//                new ValueEventListener(){
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        listItems.removeAll(listItems);
//                        for (DataSnapshot snapshot:
//                                dataSnapshot.getChildren()){
//                            final Users itemInicio= snapshot.getValue(Users.class);

//                            Log.v("E_VALUE","Data: " + itemInicio.getNameuser());
//                            Log.v("E_VALUE","Data: " + itemInicio.getNomefanart());
//                            Log.v("E_VALUE","Data: " + itemInicio.getImagem());
//                            Log.v("E_VALUE","Data: " + itemInicio.getFavoritado());
//                            Log.v("E_VALUE","DataKey: " + itemInicio.getKeyUser() +" getUID: "+mAuth.getUid());
//                            String uid=mAuth.getUid();
//                            String bdUserUid=itemInicio.getKeyUser();
//                            if((uid.equals(bdUserUid))||bdUserUid.equals("teste")){
//                                Log.v("E_VALUE","DataKeyIFELSE: " + itemInicio.getKeyUser() +" getUID: "+mAuth.getUid());
// Toast.makeText(view.getContext(),"userID: "+itemInicio.getKeyUser(),Toast.LENGTH_SHORT).show();
//                            }
//                            else{
//                                listItems.add(itemInicio);
//                            }
//                        }
//
//                        adapter=new AdapterBusca(listItems, getActivity());
//                        recyclerView.setAdapter(adapter);
//                        ProgressBar progressFrag =(ProgressBar)view.findViewById(R.id.progressList);
//                        progressFrag.setVisibility(view.GONE);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                }
//        );






//        Fragment abaPostsDiarie = new ListPostDiarieFragment();
//        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
//        transaction.replace(R.id.containerDiarie, abaPostsDiarie).commit();

        return view;
    }

    private void iniciarVars(View v){
        inpBusca= (EditText)v.findViewById(R.id.inpBusca);
        recyclerView = (RecyclerView)v.findViewById(R.id.rcBusca);
    }

    private void iniciarFire(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
    }

    private void editEvent(){
        inpBusca.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String palavra=inpBusca.getText().toString().trim();
                searchWorld(palavra);
            }
        });
    }

    private void searchWorld(String world){
        Query query;
        if (world.equals("")){
            query = mDatabase.child("users").orderByChild("nameuser");
        }else{
            query=mDatabase.child("users").orderByChild("nameuser").startAt(world).endAt(world+"\uf8ff");
        }

        listItems.removeAll(listItems);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                    Users itemUsers=snapshot.getValue(Users.class);

                    String uid=mAuth.getUid();
                    String bdUserUid=itemUsers.getKeyUser();
                    if((uid.equals(bdUserUid))||bdUserUid.equals("teste")){
                        Log.v("E_VALUE","DataKeyIFELSE: " + itemUsers.getKeyUser() +" getUID: "+mAuth.getUid());
// Toast.makeText(view.getContext(),"userID: "+itemInicio.getKeyUser(),Toast.LENGTH_SHORT).show();
                    }
                    else{
                        listItems.add(itemUsers);
                    }
                }
                adapter=new AdapterBusca(listItems, getActivity());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        searchWorld("");
    }
}
