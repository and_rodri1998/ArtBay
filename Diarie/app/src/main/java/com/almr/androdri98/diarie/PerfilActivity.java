package com.almr.androdri98.diarie;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.almr.androdri98.diarie.modelFirebase.Posts;
import com.almr.androdri98.diarie.modelFirebase.Users;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PerfilActivity extends AppCompatActivity {

    private FirebaseStorage storage;
    private StorageReference storageRef, imageRef;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private UploadTask uploadTask;
    private Uri uriImgFire;
    private int quantPosts=0;

    private static final int GALLERY_REQUEST = 1;

    private int isConect=0;
    private ImageView imPerf;
    private EditText editDesc;

    private int ftUpCheck=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        isConnect();
        if (isConect==1){

            ImageView round = (ImageView)findViewById(R.id.imPerf);

            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.perfil);
            RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
            roundedBitmapDrawable.setCircular(true);
            round.setImageDrawable(roundedBitmapDrawable);

            final FrameLayout abasPerf=(FrameLayout)findViewById(R.id.abasPerf);
            final String[] abaVisible = {"false"};
//        abasPerf.setVisibility(View.GONE);

            final ImageButton btnActPostPerf=(ImageButton)findViewById(R.id.btnActPostPerf);
            final ImageButton btnActPostFavorite=(ImageButton)findViewById(R.id.btnActPostFavorite);

            final ImageButton btnEdit=(ImageButton)findViewById(R.id.btnEdit);
            final ImageButton btnFtEdit=(ImageButton)findViewById(R.id.editFtPerf);
            final Button btnSalvar=(Button)findViewById(R.id.btnSalvar);
            final Button btnCancelar=(Button)findViewById(R.id.btnCancelar);
            final TextView expSaveAlt=(TextView)findViewById(R.id.expText);
            editDesc=(EditText)findViewById(R.id.editDesc);
            final TextView descPerf=(TextView)findViewById(R.id.descPerf);
            imPerf=(ImageView)findViewById(R.id.imPerf);

            btnSalvar.setVisibility(View.GONE);
            btnCancelar.setVisibility(View.GONE);
            expSaveAlt.setVisibility(View.GONE);
            btnFtEdit.setVisibility(View.GONE);
            editDesc.setVisibility(View.GONE);

            btnEdit.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(v.getContext(),"Editar perfil",Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    descPerf.setVisibility(View.GONE);
                    btnEdit.setVisibility(View.GONE);

                    btnSalvar.setVisibility(View.VISIBLE);
                    btnCancelar.setVisibility(View.VISIBLE);
                    expSaveAlt.setVisibility(View.VISIBLE);
                    btnFtEdit.setVisibility(View.VISIBLE);
                    editDesc.setVisibility(View.VISIBLE);
                }
            });

            btnCancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnSalvar.setVisibility(View.GONE);
                    btnCancelar.setVisibility(View.GONE);
                    expSaveAlt.setVisibility(View.GONE);
                    btnFtEdit.setVisibility(View.GONE);
                    editDesc.setVisibility(View.GONE);

                    descPerf.setVisibility(View.VISIBLE);
                    btnEdit.setVisibility(View.VISIBLE);
                }
            });

            btnSalvar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                if(uriImgFire==null&&editDesc.length()==0){
                    Toast.makeText(PerfilActivity.this,"Existem campos vazios",Toast.LENGTH_SHORT).show();
                }
                else {
                    uploadPerfil();
                    btnSalvar.setVisibility(View.GONE);
                    btnCancelar.setVisibility(View.GONE);
                    expSaveAlt.setVisibility(View.GONE);
                    btnFtEdit.setVisibility(View.GONE);
                    editDesc.setVisibility(View.GONE);

                    descPerf.setVisibility(View.VISIBLE);
                    btnEdit.setVisibility(View.VISIBLE);
                }
                }
            });

            btnFtEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent galleryOpen = new Intent(Intent.ACTION_GET_CONTENT);
                    galleryOpen.setType("image/*");
                    startActivityForResult(galleryOpen, GALLERY_REQUEST);

                }
            });
            
            mDatabase = FirebaseDatabase.getInstance().getReference();
            mAuth = FirebaseAuth.getInstance();
            storage=FirebaseStorage.getInstance();
            storageRef=storage.getReference();

            mDatabase.child("users").child(mAuth.getUid()).addValueEventListener(
                    new ValueEventListener(){
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            Users itemInicio= dataSnapshot.getValue(Users.class);

                            Log.v("E_VALUE","Data username: " + itemInicio.getNameuser());

                            Picasso.with(PerfilActivity.this).load(itemInicio.getFtperfil()).resize(500,500).centerCrop().into(imPerf);

                            TextView tvUsername=(TextView)findViewById(R.id.tvUsername);
                            tvUsername.setText(itemInicio.getNameuser());

                            descPerf.setText(itemInicio.getDescricao());

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
            );

            mDatabase.child("posts").child(mAuth.getUid()).addValueEventListener(
                    new ValueEventListener(){
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot snapshot:
                                    dataSnapshot.getChildren()){
//                            Posts itemInicio= snapshot.getValue(Posts.class);
                                quantPosts++;
                                TextView quant=(TextView)findViewById(R.id.qPost);
                                quant.setText(String.valueOf(quantPosts));
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
            );

            AbaPostsFragment fragment = new AbaPostsFragment();
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.abasPerf, fragment);
            ft.commit();

//      Visualização dos posts
            btnActPostPerf.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
//                if(abaVisible[0] =="false"){
//                    abasPerf.setVisibility(View.VISIBLE);
//                    abaVisible[0] ="true";
//                }

                    AbaPostsFragment fragment = new AbaPostsFragment();
                    android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.abasPerf, fragment);
                    ft.commit();
                }
            });

            btnActPostFavorite.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
//                if(abaVisible[0] =="false"){
//                    abasPerf.setVisibility(View.VISIBLE);
//                    abaVisible[0] ="true";
//                }

                    AbaFavoriteFragment fragment = new AbaFavoriteFragment();
                    android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.abasPerf, fragment);
                    ft.commit();
                }
            });

//      só as informações toasts
            btnActPostPerf.setOnLongClickListener(new View.OnLongClickListener(){
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(v.getContext(),"Visualizar Posts",Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            btnActPostFavorite.setOnLongClickListener(new View.OnLongClickListener(){
                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(v.getContext(),"Visualizar Favoritos",Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

        }else if(isConect==0){
            Intent intent=new Intent(PerfilActivity.this, ErrorDisplayActivity.class);
            finish();
            startActivity(intent);
        }
    }

    public void isConnect(){
        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager.getActiveNetworkInfo()!= null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()){
            isConect=1;
        }else{
            isConect=0;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(resultCode, resultCode, data);
        switch (requestCode){
            case GALLERY_REQUEST:
                if(resultCode == RESULT_OK){
                    Uri imageUri = data.getData();
                    uriImgFire=data.getData();
                    imPerf.setImageURI(imageUri);
                }
                break;
        }
    }

    public void uploadPerfil(){

        if (uriImgFire==null){
            Toast.makeText(PerfilActivity.this,"Alterando descrição",Toast.LENGTH_SHORT).show();
            String descricao = editDesc.getText().toString();
            mDatabase.child("users").child(mAuth.getUid()).child("descricao").setValue(descricao);
        }else if(editDesc.length()==0){
            Toast.makeText(PerfilActivity.this,"Alterando fotos",Toast.LENGTH_SHORT).show();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            Date hora = Calendar.getInstance().getTime();
            String dataFormatada = sdf.format(hora);

            MessageDigest m = null;
            try {
                m = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            m.update(dataFormatada.getBytes(), 0, dataFormatada.length());
            String namePhoto = new BigInteger(1, m.digest()).toString(16);

            imageRef = storageRef.child(mAuth.getUid()).child("perfil").child(namePhoto);

            uploadTask = imageRef.putFile(uriImgFire);

            uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.v("E_VALUE:", String.valueOf(100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount()));
                }
            });

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "upload falhou", Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    Uri downloadUri = taskSnapshot.getDownloadUrl();
                    final String uriImagem = downloadUri.toString();

                    Log.v("E_VALUE:", "UPLOAD CONCLUIDO;" + uriImagem);

                    mDatabase.child("users").child(mAuth.getUid()).child("ftperfil").setValue(uriImagem);
                }
            });
        }
        else{
            Toast.makeText(PerfilActivity.this,"Alterando dados do perfil",Toast.LENGTH_SHORT).show();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            Date hora = Calendar.getInstance().getTime();
            String dataFormatada = sdf.format(hora);

            MessageDigest m = null;
            try {
                m = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            m.update(dataFormatada.getBytes(), 0, dataFormatada.length());
            String namePhoto = new BigInteger(1, m.digest()).toString(16);

            imageRef = storageRef.child(mAuth.getUid()).child("perfil").child(namePhoto);

            uploadTask = imageRef.putFile(uriImgFire);

            uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.v("E_VALUE:", String.valueOf(100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount()));
                }
            });

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "upload falhou", Toast.LENGTH_SHORT).show();
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    Uri downloadUri = taskSnapshot.getDownloadUrl();
                    final String uriImagem = downloadUri.toString();

                    Log.v("E_VALUE:", "UPLOAD CONCLUIDO;" + uriImagem);


                    String descricao = editDesc.getText().toString();
                    mDatabase.child("users").child(mAuth.getUid()).child("ftperfil").setValue(uriImagem);
                    mDatabase.child("users").child(mAuth.getUid()).child("descricao").setValue(descricao);
                }
            });
        }


    }
}
