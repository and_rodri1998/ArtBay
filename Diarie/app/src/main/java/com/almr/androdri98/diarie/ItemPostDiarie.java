package com.almr.androdri98.diarie;

/**
 * Created by androdri98 on 01/02/18.
 */

public class ItemPostDiarie {
    private String usernameDiarie;
    private int imagemUser;
    private String descPostuser;
    private String autorCitacao;

    public ItemPostDiarie(String usernameDiarie, int imagemUser, String descPostuser, String autorCitacao) {
        this.usernameDiarie = usernameDiarie;
        this.imagemUser = imagemUser;
        this.descPostuser = descPostuser;
        this.autorCitacao=autorCitacao;
    }

    public String getUsernameDiarie() {
        return usernameDiarie;
    }

    public void setUsernameDiarie(String usernameDiarie) {
        this.usernameDiarie = usernameDiarie;
    }

    public int getImagemUser() {
        return imagemUser;
    }

    public void setImagemUser(int imagemUser) {
        this.imagemUser = imagemUser;
    }

    public String getDescPostuser() {
        return descPostuser;
    }

    public void setDescPostuser(String descPostuser) {
        this.descPostuser = descPostuser;
    }

    public String getAutorCitacao() {
        return autorCitacao;
    }

    public void setAutorCitacao(String autorCitacao) {
        this.autorCitacao = autorCitacao;
    }
}
