package com.almr.androdri98.diarie;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.almr.androdri98.diarie.modelFirebase.Posts;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Random;

/**
 * Created by androdri8 on 08/02/18.
 */

public class GradbkLoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final String TAG="SingupActivity";
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

	    mAuth = FirebaseAuth.getInstance();
	    FirebaseUser currentUser=mAuth.getCurrentUser();
	    if (currentUser==null){
            setContentView(R.layout.activity_login_gradbk);
            getSupportActionBar().hide();

            Button btnLogin=(Button)findViewById(R.id.btnLogin);
            Button btnCad=(Button)findViewById(R.id.btnCad);
            final EditText editEmail=(EditText)findViewById(R.id.inpUsername);
            final EditText editSenha=(EditText)findViewById(R.id.inpSenha);
            btnLogin.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    if((editEmail.getText().length()==0)||(editSenha.getText().length()==0)){
                        Toast.makeText(v.getContext(),"existem campos vazios",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        String inpEmail=editEmail.getText().toString();
                        String inpSenha=editSenha.getText().toString();
                        signIn(inpEmail,inpSenha);
                    }
                }
            });

            btnCad.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), SignupActivity.class);
                    startActivity(intent);
                }
            });
        }
        else{
            Intent intent=new Intent(this,MainActivity.class);
            startActivity(intent);
            GradbkLoginActivity.this.finish();
        }


    }

    private void signIn(String email, String password) {

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            String uid =mAuth.getUid();
                            Toast.makeText(GradbkLoginActivity.this, "Seja bem vindo!",
                                    Toast.LENGTH_LONG).show();
                            mDatabase = FirebaseDatabase.getInstance().getReference();

//                            Posts post;
//                            Random geraRan= new Random();
//                            post = new Posts(0,"imagem teste","FanArte One","UserFanarteiro");

//                            mDatabase.child("posts").child(uid).child("post00").setValue(post);

                            Intent intent=new Intent(GradbkLoginActivity.this,MainActivity.class);
                            startActivity(intent);
                            GradbkLoginActivity.this.finish();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(GradbkLoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        // [END sign_in_with_email]
    }
}
