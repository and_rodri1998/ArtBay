package com.almr.androdri98.diarie;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by androdri98 on 01/02/18.
 */

public class AdapterPostDiarie extends RecyclerView.Adapter<AdapterPostDiarie.ViewHolder> {
    private List<ItemPostDiarie> itemPostDiaries;
    private Context context;

    public AdapterPostDiarie(List<ItemPostDiarie> itemPostDiaries, Context context) {
        this.itemPostDiaries = itemPostDiaries;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.post_diarie, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ItemPostDiarie itemPost = itemPostDiaries.get(position);

        holder.tvUser.setText(itemPost.getUsernameDiarie());
        holder.imUser.setImageResource(itemPost.getImagemUser());
        holder.autorDesc.setText(itemPost.getAutorCitacao());

        holder.itemPostClick.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(v.getContext(), ViewPostActivity.class);
                context.startActivity(intent);
//                AppCompatActivity activity =  (AppCompatActivity) v.getContext();
//                ViewPostFragment fragment = new ViewPostFragment();
//                activity.getSupportFragmentManager().beginTransaction().replace(R.id.content, fragment).addToBackStack(null).commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemPostDiaries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvUser;
        public ImageView imUser;
        public TextView tvDesc;
        public TextView autorDesc;
        ConstraintLayout itemPostClick;

        public ViewHolder(View itemView) {
            super(itemView);

            tvUser=(TextView)itemView.findViewById(R.id.tvUser);
            imUser=(ImageView)itemView.findViewById(R.id.imUser);
            tvDesc=(TextView)itemView.findViewById(R.id.tvNameFan);
            autorDesc=(TextView)itemView.findViewById(R.id.tvNameFan);
            itemPostClick = (ConstraintLayout) itemView.findViewById(R.id.postDiarie);

        }
    }
}
