package com.almr.androdri98.diarie;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class ErrorDisplayActivity extends AppCompatActivity {

    private int isConect=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_display);

        SwipeRefreshLayout swipe=(SwipeRefreshLayout)findViewById(R.id.swiperefresh);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isConnect();
                if (isConect==1){
                    Intent intent=new Intent(ErrorDisplayActivity.this, MainActivity.class);
                    finish();
                    startActivity(intent);
                }else if(isConect==0){
                    Toast.makeText(ErrorDisplayActivity.this,"Continua sem internet",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void isConnect(){
        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager.getActiveNetworkInfo()!= null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()){
            isConect=1;
        }else{
            isConect=0;
        }
    }
}
