package com.almr.androdri98.diarie;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.almr.androdri98.diarie.modelFirebase.Posts;
import com.almr.androdri98.diarie.modelFirebase.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Random;

public class SignupActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final String TAG="SingupActivity";
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cadastro");

        Button btnCad=(Button)findViewById(R.id.btnCad);

        mAuth = FirebaseAuth.getInstance();

        final EditText editUser=(EditText)findViewById(R.id.inpUsername);
        final EditText editEmail=(EditText)findViewById(R.id.inpEmail);
        final EditText editSenha=(EditText)findViewById(R.id.inpSenha);

        btnCad.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if ((editEmail.getText().length()==0)||(editSenha.getText().length()==0)||(editUser).getText().length()==0){
                    Toast.makeText(v.getContext(),"Existem campos vazios",Toast.LENGTH_SHORT).show();
                }
                else{
                    String inpUser=editUser.getText().toString();
                    String inpEmail=editEmail.getText().toString();
                    String inpSenha=editSenha.getText().toString();

                    createAccount(inpEmail,inpSenha);
                    signIn(inpEmail,inpSenha,inpUser);
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            super.onBackPressed();
        }
        return true;
    }

    private void createAccount(final String email, final String password) {
        Log.d(TAG, "createAccount:" + email);

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            Toast.makeText(SignupActivity.this, "Cadastrado com sucesso.",
                                    Toast.LENGTH_SHORT).show();
                            FirebaseUser user = mAuth.getCurrentUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignupActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void signIn(final String email, final String password, final String nameuser) {

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(SignupActivity.this, "Seja Bem vindo!",
                                    Toast.LENGTH_SHORT).show();

                            String uid=mAuth.getUid();

                            mDatabase = FirebaseDatabase.getInstance().getReference();

                            Users usersFire;
                            Random geraRan= new Random();
                            usersFire = new Users(email,nameuser,uid,"http://i.imgur.com/DvpvklR.png","Hello's seja bem vindo ao mundo mágico das FanArts!");

                            mDatabase.child("users").child(uid).setValue(usersFire);
                            mDatabase.child("seguidores").child(uid).child("uid").setValue("saveProfile");

                            Intent intent=new Intent(SignupActivity.this,MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            SignupActivity.this.finish();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(SignupActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        // [END sign_in_with_email]
    }


}
