package com.almr.androdri98.diarie;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.almr.androdri98.diarie.modelFirebase.Posts;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FeedFragment extends Fragment {

    private RecyclerView recyclerView;
    private AdapterPostFeed adapter;
    private List<Posts> listItems;
    private String text;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    public FeedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed, container, false);

//        ImageView round = (ImageView)view.findViewById(R.id.imgPerf);
//
//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.perfil);
//        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
//        roundedBitmapDrawable.setCircular(true);
//        round.setImageDrawable(roundedBitmapDrawable);

        mAuth = FirebaseAuth.getInstance();

        recyclerView = (RecyclerView)view.findViewById(R.id.rcFeed);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        listItems=new ArrayList<>();

//        for (int i=0; i<=20; i++){
//            text =getString(R.string.text_teste);
//            ItemPostDiarie listItem=new ItemPostDiarie(
//                    "User"+(i+1),
//                    R.drawable.perfil,
//                    text,
//                    "- Elliot Alderson"
//            );
//
//            listItems.add(listItem);
//        }

        adapter=new AdapterPostFeed(listItems, getActivity());

        recyclerView.setAdapter(adapter);

//        Fragment abaPostsFeed= new ListPostFeedFragment();
//        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
//        transaction.replace(R.id.containerFeed, abaPostsFeed).commit();


        final Context context = view.getContext();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("seguidores").child(mAuth.getUid()).addValueEventListener(
                new ValueEventListener(){
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        listItems.removeAll(listItems);
                        for (DataSnapshot snapshot:
                                dataSnapshot.getChildren()){
                            String keyUser= snapshot.getValue(String.class);

                            Log.v("E_VALUE","Data: " + keyUser);

                            mDatabase.child("posts").child(keyUser).addValueEventListener(
                                new ValueEventListener(){
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
    //                                    listItems.removeAll(listItems);
                                        for (DataSnapshot snapshot:
                                                dataSnapshot.getChildren()){
                                            Posts itemInicio= snapshot.getValue(Posts.class);

                                            Log.v("E_VALUE","Data: " + itemInicio.getUsernamepost());
                                            Log.v("E_VALUE","Data: " + itemInicio.getNomefanart());
                                            Log.v("E_VALUE","Data: " + itemInicio.getImagem());
                                            Log.v("E_VALUE","Data: " + itemInicio.getFavoritado());

                                            listItems.add(itemInicio);
                                        }
                                        adapter.notifyDataSetChanged();
                //                        ProgressBar progressFrag =(ProgressBar)view.findViewById(R.id.progressList);
                //                        progressFrag.setVisibility(view.GONE);
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                }
                            );

                        }
//                        adapter.notifyDataSetChanged();
//                        ProgressBar progressFrag =(ProgressBar)view.findViewById(R.id.progressList);
//                        progressFrag.setVisibility(view.GONE);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
















//        mDatabase.child("posts").child(mAuth.getUid()).addValueEventListener(
//                new ValueEventListener(){
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        listItems.removeAll(listItems);
//                        for (DataSnapshot snapshot:
//                                dataSnapshot.getChildren()){
//                            Posts itemInicio= snapshot.getValue(Posts.class);
//
//                            Log.v("E_VALUE","Data: " + itemInicio.getUsernamepost());
//                            Log.v("E_VALUE","Data: " + itemInicio.getNomefanart());
//                            Log.v("E_VALUE","Data: " + itemInicio.getImagem());
//                            Log.v("E_VALUE","Data: " + itemInicio.getFavoritado());
//
//                            listItems.add(itemInicio);
//                        }
//                        adapter.notifyDataSetChanged();
////                        ProgressBar progressFrag =(ProgressBar)view.findViewById(R.id.progressList);
////                        progressFrag.setVisibility(view.GONE);
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                }
//        );


        return view;
    }

}
