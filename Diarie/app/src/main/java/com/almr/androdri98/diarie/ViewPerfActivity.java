package com.almr.androdri98.diarie;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.almr.androdri98.diarie.modelFirebase.Posts;
import com.almr.androdri98.diarie.modelFirebase.Users;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ViewPerfActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private int seg;

    private int isConect=0;
    private int quantPosts=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_perf);

        isConnect();
        if (isConect==1){

            ImageView round = (ImageView)findViewById(R.id.imPerf);
            TextView tvUser=(TextView)findViewById(R.id.tvUsername);

            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.perfil);
            RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
            roundedBitmapDrawable.setCircular(true);
            round.setImageDrawable(roundedBitmapDrawable);

            final FrameLayout abasPerf=(FrameLayout)findViewById(R.id.abasPerf);
            final String[] abaVisible = {"false"};
            abasPerf.setVisibility(View.GONE);

            final ImageButton btnActPostPerf=(ImageButton)findViewById(R.id.btnActPostPerf);
            final ImageButton btnSeguir=(ImageButton)findViewById(R.id.btnSeguir);
            ImageView imUser=(ImageView)findViewById(R.id.imPerf);
            TextView tvDesc=(TextView)findViewById(R.id.descPerf);
            final TextView quant=(TextView)findViewById(R.id.qPost);

            Intent intent=getIntent();
            Bundle recDadosPost=new Bundle();
            recDadosPost=intent.getExtras();

            String nome=recDadosPost.getString("nameUser").toString();
            final String idAutPost=recDadosPost.getString("idAutPost").toString();
            String uriImagemPerf=recDadosPost.getString("uriImgPerf").toString();
            String descricao=recDadosPost.getString("descricao").toString();

            tvUser.setText(nome);
            tvDesc.setText(descricao);
            Picasso.with(ViewPerfActivity.this).load(uriImagemPerf).resize(500,500).centerCrop().into(imUser);

            mDatabase = FirebaseDatabase.getInstance().getReference();
            mAuth = FirebaseAuth.getInstance();

            mDatabase.child("posts").child(idAutPost).addValueEventListener(
                    new ValueEventListener(){
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot snapshot:
                                    dataSnapshot.getChildren()){
//                            Posts itemInicio= snapshot.getValue(Posts.class);
                                quantPosts++;
                                quant.setText(String.valueOf(quantPosts));
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
            );

            mDatabase.child("seguidores").child(mAuth.getUid()).addListenerForSingleValueEvent(
                    new ValueEventListener(){
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot snapshot:
                                    dataSnapshot.getChildren()){
                                String itemInicio= snapshot.getValue(String.class);

//                            Log.v("E_VALUE","Data: " + itemInicio.getNameuser());
//                            Log.v("E_VALUE","Data: " + itemInicio.getNomefanart());
//                            Log.v("E_VALUE","Data: " + itemInicio.getImagem());
//                            Log.v("E_VALUE","Data: " + itemInicio.getFavoritado());
                                String uidSeg=itemInicio;
                                if (uidSeg.equals(idAutPost)){
                                    Log.v("E_VALUE","DataKey: " + itemInicio);
                                    btnSeguir.setImageResource(R.drawable.ic_done_black_44dp);
                                    seg=1;
                                    funcBtnSeguir();
                                }
                                else{
                                    if (!(seg==1)){
                                        seg=0;
                                        funcBtnSeguir();
                                    }
                                }
                            }
//                        ProgressBar progressFrag =(ProgressBar)view.findViewById(R.id.progressList);
//                        progressFrag.setVisibility(view.GONE);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
            );
//      Visualização dos posts
            btnActPostPerf.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    if(abaVisible[0] =="false"){
                        abasPerf.setVisibility(View.VISIBLE);
                        abaVisible[0] ="true";
                    }

                    ViewPerfPostsFragment fragment = new ViewPerfPostsFragment();
                    Bundle data=new Bundle();
                    data.putString("keyPerf",idAutPost);
                    fragment.setArguments(data);
                    android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.abasPerf, fragment);
                    ft.commit();
                }
            });

            btnActPostPerf.setOnLongClickListener(new View.OnLongClickListener(){

                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(v.getContext(),"Visualizar Posts",Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            btnSeguir.setOnLongClickListener(new View.OnLongClickListener(){

                @Override
                public boolean onLongClick(View v) {
                    Toast.makeText(v.getContext(),"Seguir",Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

        }else if(isConect==0){
            Intent intent=new Intent(ViewPerfActivity.this, ErrorDisplayActivity.class);
            finish();
            startActivity(intent);
        }

    }

    protected void funcBtnSeguir(){

        Intent intent=getIntent();
        Bundle recDadosPost=new Bundle();
        recDadosPost=intent.getExtras();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        final String idAutPost=recDadosPost.getString("idAutPost").toString();
        final ImageButton btnSeguir=(ImageButton)findViewById(R.id.btnSeguir);

        if (seg==1){
            btnSeguir.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Toast.makeText(v.getContext(),"deixando de Seguir",Toast.LENGTH_SHORT).show();

                    mDatabase.child("seguidores").child(mAuth.getUid()).child(idAutPost).setValue(null);
                    btnSeguir.setImageResource(R.drawable.ic_add_black_24dp_menu);
                    seg=0;
                    funcBtnSeguir();
                }
            });
        }else if(seg==0){
            btnSeguir.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    Toast.makeText(v.getContext(),"Seguindo",Toast.LENGTH_SHORT).show();
                    String uid=mAuth.getUid();
                    mDatabase.child("seguidores").child(uid).child(idAutPost).setValue(idAutPost);
                    btnSeguir.setImageResource(R.drawable.ic_done_black_44dp);
                    seg=1;
                    funcBtnSeguir();
                }
            });
        }
    }

    public void isConnect(){
        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager.getActiveNetworkInfo()!= null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()){
            isConect=1;
        }else{
            isConect=0;
        }
    }
}
