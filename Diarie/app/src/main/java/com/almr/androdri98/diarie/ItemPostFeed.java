package com.almr.androdri98.diarie;

/**
 * Created by androdri98 on 31/01/18.
 */

public class ItemPostFeed {
    private String usernameUser;
    private int imagemUser;

    public ItemPostFeed(int imagem, String username) {
        this.imagemUser = imagem;
        this.usernameUser = username;
    }

    public String getUsernameUser() {
        return usernameUser;
    }

    public void setUsernameUser(String usernameUser) {
        this.usernameUser = usernameUser;
    }

    public int getImagemUser() {
        return imagemUser;
    }

    public void setImagemUser(int imagemUser) {
        this.imagemUser = imagemUser;
    }
}
