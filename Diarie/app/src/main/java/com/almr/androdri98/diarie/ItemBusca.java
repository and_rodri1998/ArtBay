package com.almr.androdri98.diarie;

/**
 * Created by androdri98 on 10/02/18.
 */

public class ItemBusca {
    private String usernameBusca;
    private int imagemUser;

    public ItemBusca(String usernameBusca, int imagemUser) {
        this.usernameBusca = usernameBusca;
        this.imagemUser = imagemUser;
    }

    public String getUsernameBusca() {
        return usernameBusca;
    }

    public void setUsernameBusca(String usernameBusca) {
        this.usernameBusca = usernameBusca;
    }

    public int getImagemUser() {
        return imagemUser;
    }

    public void setImagemUser(int imagemUser) {
        this.imagemUser = imagemUser;
    }
}
