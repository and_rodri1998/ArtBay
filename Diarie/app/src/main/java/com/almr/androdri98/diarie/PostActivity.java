package com.almr.androdri98.diarie;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.almr.androdri98.diarie.modelFirebase.Posts;
import com.almr.androdri98.diarie.modelFirebase.Users;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.math.BigInteger;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PostActivity extends AppCompatActivity {

    private ImageButton selectimage;
    private ImageView previewImage;
    private static final int GALLERY_REQUEST = 1;
    private static final int SAVE_IMAGEM = 2;

    private FirebaseStorage storage;
    private StorageReference storageRef, imageRef;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private UploadTask uploadTask;
    private Uri uriImgFire;
    private Intent dadosImagem;
    EditText editFan;

    private int isConect=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        isConnect();
        if (isConect==1){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Crie sua FanArt");

            selectimage = (ImageButton) findViewById(R.id.imSelect);
            previewImage =(ImageView) findViewById(R.id.imPrev);
            storage=FirebaseStorage.getInstance();
            storageRef=storage.getReference();

            previewImage.setVisibility(View.GONE);

            selectimage.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    Intent galleryOpen = new Intent(Intent.ACTION_GET_CONTENT);
                    galleryOpen.setType("image/*");
                    startActivityForResult(galleryOpen, GALLERY_REQUEST);
                }
            });

            FloatingActionButton fab= (FloatingActionButton)findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(final View v){

                    editFan=(EditText)findViewById(R.id.nmFarArt);
                    if(editFan.getText().length()==0){
                        Toast.makeText(v.getContext(),"Existem campos vazios",Toast.LENGTH_SHORT).show();
                    }else{

                        uploadFoto();

                    }
                }
            });
        }else if(isConect==0){
            Intent intent=new Intent(PostActivity.this, ErrorDisplayActivity.class);
            finish();
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            super.onBackPressed();
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(resultCode, resultCode, data);
        switch (requestCode){
            case GALLERY_REQUEST:
                if(resultCode == RESULT_OK){
                    Uri imageUri = data.getData();
                    uriImgFire=data.getData();
                    previewImage.setImageURI(imageUri);
                    previewImage.setVisibility(View.VISIBLE);
//                    uploadFoto();
                }
                break;
        }
    }

    public void uploadFoto(){

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date hora = Calendar.getInstance().getTime();
        String dataFormatada = sdf.format(hora);

        MessageDigest m= null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        m.update(dataFormatada.getBytes(),0,dataFormatada.length());
        String namePhoto=new BigInteger(1,m.digest()).toString(16);

        imageRef=storageRef.child(mAuth.getUid()).child(namePhoto);

        uploadTask=imageRef.putFile(uriImgFire);

        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                Log.v("E_VALUE:",String.valueOf(100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount()));
            }
        });

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "upload falhou",Toast.LENGTH_SHORT).show();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                Uri downloadUri=taskSnapshot.getDownloadUrl();
                final String uriImagem=downloadUri.toString();

                Log.v("E_VALUE:", "UPLOAD CONCLUIDO;" + uriImagem);

                final String nomeFan=editFan.getText().toString();

                mDatabase.child("users").child(mAuth.getUid()).child("nameuser").addValueEventListener(
                        new ValueEventListener(){
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
//                                    for (DataSnapshot snapshot:
//                                            dataSnapshot.getChildren()){
                                String itemUser= dataSnapshot.getValue(String.class);

                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                                Date hora = Calendar.getInstance().getTime();
                                String dataFormatada = sdf.format(hora);

                                MessageDigest m= null;
                                try {
                                    m = MessageDigest.getInstance("MD5");
                                } catch (NoSuchAlgorithmException e) {
                                    e.printStackTrace();
                                }
                                m.update(dataFormatada.getBytes(),0,dataFormatada.length());
                                String namePost=new BigInteger(1,m.digest()).toString(16);

                                Posts posts = new Posts(0,"imagem teste","- "+nomeFan, itemUser,mAuth.getUid(), namePost,uriImagem);

                                mDatabase.child("posts").child(mAuth.getUid()).child(namePost).setValue(posts);

                                Log.v("E_VALUE","Data: " + itemUser);
                                Toast.makeText(getApplicationContext(),"Salvando...",Toast.LENGTH_SHORT).show();

                                dadosImagem=new Intent(getApplicationContext(), MainActivity.class);
                                dadosImagem.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(dadosImagem);
//                                    }
//                        ProgressBar progressFrag =(ProgressBar)view.findViewById(R.id.progressList);
//                        progressFrag.setVisibility(view.GONE);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        }
                );

            }
        });
    }

    public void isConnect(){
        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager.getActiveNetworkInfo()!= null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()){
            isConect=1;
        }else{
            isConect=0;
        }
    }
}


