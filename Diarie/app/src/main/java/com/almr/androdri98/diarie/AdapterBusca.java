package com.almr.androdri98.diarie;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.almr.androdri98.diarie.modelFirebase.Users;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by androdri98 on 10/02/18.
 */

public class AdapterBusca extends RecyclerView.Adapter<AdapterBusca.ViewHolder> {
    private List<Users> itensBuscas;
    private Context context;

    public AdapterBusca(List<Users> itensBuscas, Context context) {
        this.itensBuscas = itensBuscas;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_busca, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Users itemBusca = itensBuscas.get(position);

        holder.tvUser.setText(itemBusca.getNameuser());
        Picasso.with(context).load(itemBusca.getFtperfil()).resize(500,500).centerCrop().into(holder.imUser);

        holder.itemClick.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                Intent intent = new Intent(context, ViewPerfActivity.class);

                Bundle dadosPost=new Bundle();
                dadosPost.putString("uriImgPerf",itemBusca.getFtperfil());
                dadosPost.putString("descricao",itemBusca.getDescricao());
                dadosPost.putString("nameUser",itemBusca.getNameuser());
                dadosPost.putString("idAutPost",itemBusca.getKeyUser());
                intent.putExtras(dadosPost);

                context.startActivity(intent);


            }
        });

    }

    @Override
    public int getItemCount() {
        return itensBuscas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imUser;
        TextView tvUser;
        ConstraintLayout itemClick;

        public ViewHolder(View itemView) {
            super(itemView);

            imUser=(ImageView)itemView.findViewById(R.id.imUser);
            tvUser=(TextView)itemView.findViewById(R.id.tvUser);
            itemClick=(ConstraintLayout)itemView.findViewById(R.id.headUser);
        }
    }
}
