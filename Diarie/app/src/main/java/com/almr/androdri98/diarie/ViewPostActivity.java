package com.almr.androdri98.diarie;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

public class ViewPostActivity extends AppCompatActivity {

    private String TAG ="ViewPostActivity";
    private int isConect=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);

        isConnect();
        if (isConect==1){

            Intent intent=getIntent();
            Bundle recDadosPost=new Bundle();
            recDadosPost=intent.getExtras();

            TextView tvUser=(TextView)findViewById(R.id.tvUser);
            TextView tvNameFan=(TextView)findViewById(R.id.tvNameFan);
            TextView tvnumFav=(TextView)findViewById(R.id.tvFav);
            ImageView imageUser=(ImageView)findViewById(R.id.imUser);
            ImageView imFanart=(ImageView)findViewById(R.id.imFanart);

            String nome=recDadosPost.getString("nameUser").toString();
            String numFav=String.valueOf(recDadosPost.getInt("numFav"));
            String uriImagem=recDadosPost.getString("uriImagem").toString();
            String imUser=recDadosPost.getString("imUser").toString();
            String nomeFanart=recDadosPost.getString("nomeFanart").toString();
            String idAutPost=recDadosPost.getString("idAutPost").toString();
            String idPub=recDadosPost.getString("idPub").toString();

//        Log.w(TAG, "EEEEname user: "+idPub);

            tvUser.setText(nome);
            tvNameFan.setText(nomeFanart);
            tvnumFav.setText(numFav);
            Picasso.with(ViewPostActivity.this).load(uriImagem).into(imFanart);

        }else if(isConect==0){
            Intent intent=new Intent(ViewPostActivity.this, ErrorDisplayActivity.class);
            finish();
            startActivity(intent);
        }
    }

    public void isConnect(){
        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager.getActiveNetworkInfo()!= null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()){
            isConect=1;
        }else{
            isConect=0;
        }
    }
}
