package com.almr.androdri98.diarie;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almr.androdri98.diarie.modelFirebase.Posts;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AbaFavoriteFragment extends Fragment {

    private RecyclerView recyclerView;
    private AdapterPostFeed adapter;
    private List<Posts> listItems;
    private String text;

    public AbaFavoriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_aba_favorite, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.rcPostsFavorite);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        listItems=new ArrayList<>();

        for (int i=0; i<=20; i++){
            text =getString(R.string.text_teste);
            Posts listItem=new Posts(
                    0,
                    "teste imagem",
                    "- fanart name",
                    "Elliot Alderson",
                    "idteste",
                    "idTeste",
                    "http://i.imgur.com/DvpvklR.png"
            );

            listItems.add(listItem);
        }

        adapter=new AdapterPostFeed(listItems, getActivity());

        recyclerView.setAdapter(adapter);

        return view;
    }

}
