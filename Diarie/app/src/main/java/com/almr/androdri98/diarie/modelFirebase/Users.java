package com.almr.androdri98.diarie.modelFirebase;

/**
 * Created by androdri8 on 16/02/18.
 */

public class Users {
    public String email;
    public String nameuser;
    public String keyUser;
    public String ftperfil;
    public String descricao;

    public Users() {

    }

    public Users(String email, String nameuser, String keyUser, String ftperfil, String descricao) {
        this.email = email;
        this.nameuser = nameuser;
        this.keyUser = keyUser;
        this.ftperfil = ftperfil;
        this.descricao = descricao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNameuser() {
        return nameuser;
    }

    public void setNameuser(String nameuser) {
        this.nameuser = nameuser;
    }

    public String getKeyUser() {
        return keyUser;
    }

    public void setKeyUser(String keyUser) {
        this.keyUser = keyUser;
    }

    public String getFtperfil() {
        return ftperfil;
    }

    public void setFtperfil(String ftperfil) {
        this.ftperfil = ftperfil;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
