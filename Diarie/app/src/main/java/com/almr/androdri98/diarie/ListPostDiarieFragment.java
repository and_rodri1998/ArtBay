package com.almr.androdri98.diarie;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListPostDiarieFragment extends Fragment {

    private RecyclerView recyclerView;
    private AdapterPostDiarie adapter;
    private List<ItemPostDiarie> listItems;
    private String text;

    public ListPostDiarieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_post_diarie, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.rcDiarie);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        listItems=new ArrayList<>();

        for (int i=0; i<=20; i++){
            text =getString(R.string.text_teste);
            ItemPostDiarie listItem=new ItemPostDiarie(
                    "User"+(i+1),
                    R.drawable.perfil,
                    text,
                    "- Elliot Alderson"
            );

            listItems.add(listItem);
        }

        adapter=new AdapterPostDiarie(listItems, getActivity());

        recyclerView.setAdapter(adapter);

        return view;
    }

}
