package com.almr.androdri98.diarie;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements viewHelpers {

    private static final String TAG="MainActivity";

    private SectionPageAdapter mSectionPageAdapter;

    private ViewPager mViewPager;
    private FirebaseAuth mAuth;

    private int isConect=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isConnect();
        if (isConect==1){
            mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser=mAuth.getCurrentUser();
            if (currentUser==null){
                Intent intent=new Intent(this, GradbkLoginActivity.class);
                startActivity(intent);
            }else{
                setContentView(R.layout.activity_main);

                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                setSupportActionBar(toolbar);

                Log.d(TAG, "onCreate: iniciando");

                mSectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());

                mViewPager = (ViewPager) findViewById(R.id.container);
                setupViewPager(mViewPager);

                TabLayout tabLayout=(TabLayout) findViewById(R.id.tabs);
                tabLayout.setupWithViewPager(mViewPager);


                FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(view.getContext(), PostActivity.class);
                        startActivity(intent);
                    }
                });
            }
        }
        else{
            Intent intent=new Intent(this, ErrorDisplayActivity.class);
            finish();
            startActivity(intent);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_perf) {
            Intent intent = new Intent(this, PerfilActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void setupViewPager(ViewPager viewPager){
        SectionPageAdapter adapter = new SectionPageAdapter(getSupportFragmentManager());

        adapter.addFragment(new FeedFragment(), "FEED");
        adapter.addFragment(new DiarieFragment(), "BUSCA");
        adapter.addFragment(new ConfigFragment(), "CONFIG");

        viewPager.setAdapter(adapter);
    }

    @Override
    public void viewPost() {
        Toast.makeText(this, "clicou", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finalizarApp() {
        Intent intent=new Intent(this, GradbkLoginActivity.class);
        startActivity(intent);
        MainActivity.this.finish();
    }

    public void isConnect(){
        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager.getActiveNetworkInfo()!= null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()){
            isConect=1;
        }else{
            isConect=0;
        }
    }
}
