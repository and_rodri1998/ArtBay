package com.almr.androdri98.diarie;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.almr.androdri98.diarie.modelFirebase.Posts;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AbaPostsFragment extends Fragment {

    private RecyclerView recyclerView;
    private AdapterPostFeed adapter;
    private List<Posts> listItems;
    private String text;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    public AbaPostsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_aba_posts, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.rcPostsPerfil);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        listItems=new ArrayList<>();

//        for (int i=0; i<=20; i++){
//            text =getString(R.string.text_teste);
//            Posts listItem=new Posts(
//                    0,
//                    "teste imagem",
//                    "- fanart name",
//                    "Elliot Alderson"
//            );
//
//            listItems.add(listItem);
//        }

        adapter=new AdapterPostFeed(listItems, getActivity());

        recyclerView.setAdapter(adapter);

        final Context context = view.getContext();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        mDatabase.child("posts").child(mAuth.getUid()).addValueEventListener(
                new ValueEventListener(){
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        listItems.removeAll(listItems);
                        for (DataSnapshot snapshot:
                                dataSnapshot.getChildren()){
                            Posts itemInicio= snapshot.getValue(Posts.class);

                            Log.v("E_VALUE","Data: " + itemInicio.getUsernamepost());
                            Log.v("E_VALUE","Data: " + itemInicio.getNomefanart());
                            Log.v("E_VALUE","Data: " + itemInicio.getImagem());
                            Log.v("E_VALUE","Data: " + itemInicio.getFavoritado());

                            listItems.add(itemInicio);
                        }
                        adapter.notifyDataSetChanged();
//                        ProgressBar progressFrag =(ProgressBar)view.findViewById(R.id.progressList);
//                        progressFrag.setVisibility(view.GONE);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );


        return view;
    }

}
